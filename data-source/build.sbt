import Dependencies._

lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "com.vodafone",
      scalaVersion := "2.12.6",
      version      := "0.1.0-SNAPSHOT"
    )),
    name := "data-source",
    libraryDependencies ++= Seq(scallop, scribe, kafkaProducer, scalaTest % Test) ++ circe,
    javacOptions ++= Seq("-source", "1.8", "-target", "1.8"),
    mainClass in assembly := Some("com.vodafone.DataSource")
  )
