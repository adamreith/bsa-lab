import sbt._

object Dependencies {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.5"
  lazy val scribe = "com.outr" %% "scribe" % "2.6.0"
  lazy val kafkaProducer = "org.apache.kafka" % "kafka-clients" % "2.0.1"
  lazy val scallop = "org.rogach" %% "scallop" % "3.1.0"
  lazy val circe = Seq(
    "io.circe" %% "circe-core",
    "io.circe" %% "circe-generic",
    "io.circe" %% "circe-parser"
  ).map(_ % "0.9.3")
}
