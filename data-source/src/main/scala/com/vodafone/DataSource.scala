package com.vodafone

import java.util.Properties
import java.lang.Thread
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import scala.util.Random
import io.circe._, io.circe.generic.auto._, io.circe.parser._, io.circe.syntax._

object DataSource extends App {
  val conf = new Conf(args)

  scribe.info("Configure Kafka producer")
  val props = new Properties()
  props.put("bootstrap.servers", "localhost:9092")
  props.put("acks", "all")
  props.put("retries", "0")
  props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer")
  props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer")
  val producer = new KafkaProducer[String, String](props)
  
  while (true) {
    Thread.sleep(conf.sleepTime())
    val event = Event(
      Random.nextInt(conf.numActiveDevices()), 
      Random.nextInt(conf.numLocations()), 
      Random.nextInt(conf.numServices()))
    val record = new ProducerRecord[String, String]("input", event.asJson.noSpaces)
    scribe.info("Send a new event...")
    producer.send(record).get()
  }

  producer.close()
}
