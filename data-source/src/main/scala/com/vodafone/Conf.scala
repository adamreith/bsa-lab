package com.vodafone

import org.rogach.scallop._

class Conf(arguments: Seq[String]) extends ScallopConf(arguments) {
  val sleepTime = opt[Long](default = Some(1000L), noshort = true)
  val numActiveDevices = opt[Int](default = Some(10), noshort = true)
  val numLocations = opt[Int](default = Some(3), validate = (8092>), noshort = true)
  val numServices = opt[Int](default = Some(5), validate = (6>), noshort = true)
  verify()
}