package com.vodafone

case class Event(deviceId: Long, locationId: Long, serviceId: Long, eventType: String = "NetworkTraffic")