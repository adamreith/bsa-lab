echo "create 'location','d'" > load.out
echo "create 'service','d'" >> load.out
echo "create 'hbase:labels','f'" >> load.out
echo "exit" >> load.out

i=0
while read -r line; do
  echo $i","$line
  i=$((i+1))
done < locations.txt > location.csv

i=0
while read -r line; do
  echo $i","$line
  i=$((i+1))
done < services.txt > service.csv

hbase shell load.out

hbase org.apache.hadoop.hbase.mapreduce.ImportTsv -Dimporttsv.separator="," -Dimporttsv.columns=HBASE_ROW_KEY,d:location location file:///opt/hbase-scripts/location.csv

hbase org.apache.hadoop.hbase.mapreduce.ImportTsv -Dimporttsv.separator="," -Dimporttsv.columns=HBASE_ROW_KEY,d:service service file:///opt/hbase-scripts/service.csv
