import Dependencies._
import sbtassembly.AssemblyPlugin.autoImport.{ShadeRule, assemblyShadeRules}

lazy val root = (project in file(".")).settings(
  inThisBuild(
    List(
      organization := "com.vodafone",
      scalaVersion := "2.11.12",
      version := "0.1.0-SNAPSHOT"
    )),
  name := "Data Processing",
  libraryDependencies ++= Seq(spark,
                              sparkStreaming,
                              sparkStreamingKafka,
                              scribe,
                              hbaseClient,
                              hbaseCommon,
                              scalaTest % Test) ++ circe,
  assemblyShadeRules in assembly := Seq(
    ShadeRule.zap("org.apache.spark.unused.**").inAll
  ),
  javacOptions ++= Seq("-source", "1.8", "-target", "1.8")
)
