#!/usr/bin/env bash

spark-submit \
    --master local[*] \
    --class com.vodafone.DataProcessing \
    ../target/scala-2.11/Data\ Processing-assembly-0.1.0-SNAPSHOT.jar \
    > "app.log" 2>&1 &

echo $! > app.pid