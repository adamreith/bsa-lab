package com.vodafone

import java.util.Properties

import org.apache.hadoop.conf.Configuration
import org.apache.kafka.common.serialization.StringDeserializer

/** Configurations for HBase and Kafka
  *
  */
package object configurations {
  val kafkaProducerConf: Properties = {
    val x = new Properties()
    x.setProperty("bootstrap.servers", "kafka:9092")
    x.setProperty("group.id", "unimi")
    x.setProperty("key.serializer",
                  "org.apache.kafka.common.serialization.StringSerializer")
    x.setProperty("value.serializer",
                  "org.apache.kafka.common.serialization.StringSerializer")
    x
  }

  val kafkaConsumerConf: Map[String, Object] = Map[String, Object](
    "bootstrap.servers" -> "kafka:9092",
    "key.deserializer" -> classOf[StringDeserializer],
    "value.deserializer" -> classOf[StringDeserializer],
    "group.id" -> "unimi",
    "auto.offset.reset" -> "latest",
    "enable.auto.commit" -> (false: java.lang.Boolean)
  )

  val hbaseConf: Configuration = {
    val x = new Configuration()
    x.set("zookeeper.znode.parent", "/hbase")
    x.set("hbase.zookeeper.quorum", "zookeeper")
    x.set("hbase.zookeeper.property.clientPort", "2181")
    x
  }
}
