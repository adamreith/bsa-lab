package com.vodafone

import org.apache.hadoop.hbase.client.{Connection, ConnectionFactory, Get}
import org.apache.hadoop.hbase.TableName
import org.apache.hadoop.hbase.util.Bytes
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerRecord}
import org.apache.spark.SparkConf
import org.apache.spark.streaming.kafka010.KafkaUtils
import org.apache.spark.streaming.{Seconds, StreamingContext}
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import io.circe.generic.auto._
import io.circe.syntax._
import io.circe.parser.decode

object DataProcessing {
  /** Enrich a raw event with data available in HBase
    *
    * @param conn the HBase connection
    * @param event the raw event
    * @return the enriched event.
    */
  def enrich(conn: Connection)(event: Event): EnrichedEvent = {
    scribe.info("Retrieve location info")
    val locationTable = conn.getTable(TableName.valueOf("location"))
    val locationColumnFamily = "d"
    val locationColumnQualifier = "location"
    val locationGet = new Get(Bytes.toBytes(event.locationId.toString))
    val locationResult = Bytes.toString(
      locationTable
        .get(locationGet)
        .getValue(Bytes.toBytes(locationColumnFamily),
                  Bytes.toBytes(locationColumnQualifier)))

    scribe.info("Retrieve service info")
    val serviceTable = conn.getTable(TableName.valueOf("service"))
    val serviceColumnFamily = "d"
    val serviceColumnQualifier = "service"
    val serviceGet = new Get(Bytes.toBytes(event.serviceId.toString))
    val serviceResult = Bytes.toString(
      serviceTable
        .get(serviceGet)
        .getValue(Bytes.toBytes(serviceColumnFamily),
                  Bytes.toBytes(serviceColumnQualifier)))

    EnrichedEvent(event.deviceId,
                  locationResult,
                  serviceResult,
                  event.eventType)
  }

  /** Send an event to a Kafka topic
    *
    * @param kafkaProducer the Kafka producer
    * @param event the event to send
    * @param topic the topic in which the vent must be sent.
    */
  def sendEvent(kafkaProducer: KafkaProducer[String, String])(
      event: EnrichedEvent,
      topic: String): Unit = {
    val kafkaRecord =
      new ProducerRecord[String, String](topic, event.asJson.noSpaces)
    kafkaProducer.send(kafkaRecord)
  }

  def main(args: Array[String]): Unit = {
    val sparkConf = new SparkConf().setAppName("Lab")
    val ssc = new StreamingContext(sparkConf, Seconds(10))
    val topics = Array("input")

    val stream = KafkaUtils.createDirectStream[String, String](
      ssc,
      PreferConsistent,
      Subscribe[String, String](topics, configurations.kafkaConsumerConf))

    stream
      .map(_.value())
      .flatMap(decode[Event](_).right.toOption) // drop non parsable JSON object
      .foreachRDD {
        _ foreachPartition {
          case xs if xs.nonEmpty =>
            scribe.info("Non empty partition")
            // create a connection to HBase and Kafka producer that will be used for all events in partition
            scribe.info("Create HBase connection")
            val hbaseConn =
              ConnectionFactory.createConnection(configurations.hbaseConf)
            scribe.info("Create kafka producer")
            val kafkaProducer =
              new KafkaProducer[String, String](
                configurations.kafkaProducerConf)

            for (x <- xs) {
              val y = enrich(hbaseConn)(x)
              y.location.toLowerCase match {
                case "milano" => sendEvent(kafkaProducer)(y, "ok")
                case _        => sendEvent(kafkaProducer)(y, "ko")
              }
            }

            hbaseConn.close()
            kafkaProducer.close()

          case xs if xs.isEmpty => scribe.info("Empty partition")
        }
      }

    ssc.start()
    ssc.awaitTermination()
  }
}
