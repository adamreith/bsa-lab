package com.vodafone

case class Event(deviceId: Long,
                 locationId: Long,
                 serviceId: Long,
                 eventType: String)

case class EnrichedEvent(deviceId: Long,
                         location: String,
                         service: String,
                         eventType: String)
