import sbt._

object Dependencies {
  lazy val scalaTest = "org.scalatest" %% "scalatest" % "3.0.5"
  lazy val hbaseCommon = "org.apache.hbase" % "hbase-common" % "1.3.1"
  lazy val hbaseClient = "org.apache.hbase" % "hbase-client" % "1.3.1"
  lazy val spark = "org.apache.spark" %% "spark-core" % "2.4.0" % Provided
  lazy val sparkStreaming = "org.apache.spark" %%"spark-streaming" % "2.4.0" % Provided
  lazy val sparkStreamingKafka = "org.apache.spark" %%"spark-streaming-kafka-0-10" % "2.4.0"
  lazy val scribe = "com.outr" %% "scribe" % "2.6.0"
  lazy val circe: Seq[ModuleID] = Seq(
    "io.circe" %% "circe-core",
    "io.circe" %% "circe-generic",
    "io.circe" %% "circe-parser"
  ).map(_ % "0.9.3")

}
